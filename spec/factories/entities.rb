
FactoryGirl.define do

  factory :invalid_entity, class: UniverseCompiler::Entity::Base do
    name 'invalid'
  end

  factory :full_entity, class: TestEntity do
    name 'full'
  end

  factory :docker_config do
    name 'Your registry config'
    registry 'your.registry:5000'
    image_namespace 'ns_for_tests'
  end

  factory :base_image, class: Image do
    name 'root'
    external :busybox
  end

  factory :inherited_from_base, class: Image do
    name 'inherited'

    # after(:build) do |i|
    #   i.from = build :base_image
    # end
  end

  factory :release do
    name 'Stable release'
    association :docker_config, factory: :docker_config, strategy: :build

    images_versions do
      {
          build(:base_image) => '1.0.0',
          build(:inherited_from_base) => '0.2.1'
      }
    end
  end

  factory :universe, class: UniverseCompiler::Universe::Base do
    name 'Chaotic Universe'

    factory :universe_with_objects, class: UniverseCompiler::Universe::Base do

      # after(:build) do |universe|
      #   dc, bi, ifb, rel = build(:docker_config), build(:base_image), build(:inherited_from_base), build(:release)
      #   rel.docker_config = dc
      #   rel.images_versions = {
      #       bi => '1.0.0',
      #       ifb => '0.2.1'
      #   }
      #   universe << dc << bi << ifb << rel
      # end

    end

  end


end
