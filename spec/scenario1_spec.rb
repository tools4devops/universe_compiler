require 'spec_helper'

describe 'Scenario 1 - Real life objects' do

  subject do
    entities = get_scenario1_valid_entities
    u = build(:universe)
    entities.each { |e| u.add e}
    u
  end

  before(:each) do
    UniverseCompiler::Universe::Base.universes.clear
  end

  context 'when not giving a name to the universe' do

    subject { UniverseCompiler.new_universe }

    it 'should work as expected' do
      expect { subject.compile }.not_to raise_error
    end

  end


  it 'The universe should have a name' do
    expect(subject.name).to eq 'Chaotic Universe'
  end

  context 'when the universe is NOT correctly setup' do

    subject do
      entities = get_scenario1_invalid_entities
      u = build(:universe)
      entities.each { |e| u.add e}
      u
    end

    it 'should not be valid' do
      expect(subject).not_to be_valid
    end

    context 'when compiling the universe' do

      it 'should fail compiling the universe' do
        expect { subject.compile }.to raise_error UniverseCompiler::Error
      end
    end
  end

  context 'when the universe is correctly setup' do

    it 'should be valid' do
      expect(subject).to be_valid
    end

    it 'The universe should have a name' do
      expect(subject.name).to eq 'Chaotic Universe'
    end


    context 'when compiling the universe' do

      it 'The universe should have a name' do
        expect(subject.compile.name).to start_with 'Chaotic Universe - COMPILED #'
      end

      it 'should return a new universe' do
        new_universe = subject.compile
        expect(new_universe).to be_a UniverseCompiler::Universe::Base
        expect(new_universe.object_id).not_to eq subject.object_id
        expect(new_universe.get_entities.size).to eq subject.get_entities.size
        new_universe.get_entities.each do |entity|
          expect(entity.universe).to eq new_universe
          expect(entity).to eq subject.get_entity(entity.type, entity.name)
          expect(entity).not_to equal subject.get_entity(entity.type, entity.name)
        end
      end

      it 'should be self contained' do
        res = subject.compile
        expect(res).to be_valid
        res.get_entities.each do |entity|
          entity.class.fields_constraints.each do |field_name, constraints|
            next if entity.fields[field_name].nil?
            if constraints.keys.include? :has_one
              expect(entity.fields[field_name].universe).to eq res
            end
            if constraints.keys.include? :has_many
              entity.fields[field_name].each do |linked_entity|
                expect(linked_entity.universe).to be res
              end
            end

          end
        end
      end

      it 'should only contain fully resolved entities' do
        res = subject.compile
        res.get_entities.each do |entity|
          expect(entity).to be_fully_resolved
        end

      end

      it 'should contain the same entities' do
        res = subject.compile
        res.get_entities.each do |entity|
          original_entity = subject.get_entity *(entity.to_composite_key)
          # puts "-- Original -- #{original_entity.name} (#{original_entity.object_id})"
          # puts original_entity.fields.to_yaml
          # puts "-- Compiled -- #{entity.name} (#{entity.object_id})"
          # puts entity.fields.to_yaml
          expect(entity.fields.keys).to eq original_entity.fields.keys
        end
      end

    end
  end


end