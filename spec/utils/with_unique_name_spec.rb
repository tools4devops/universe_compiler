require 'spec_helper'

describe UniverseCompiler::Utils::WithUniqueName do

  let (:test_class_with_no_seed) do
    class TestWithoutSeed
      include UniverseCompiler::Utils::WithUniqueName
    end
    TestWithoutSeed
  end

  let(:test_class_with_seed) do
    class TestWithSeed
      include UniverseCompiler::Utils::WithUniqueName
      set_name_seed :my_test_class
    end
    TestWithSeed
  end

  subject {test_class_with_no_seed.new}

  it 'should have a default default name' do
    expect(subject.name).not_to be_nil
    expect(subject.name).to start_with described_class::ClassMethods::DEFAULT_SEED
  end

  it 'should have a definitive name' do
    expect(subject.name).to eq subject.name
  end

  context 'when there is a seed name defined' do

    subject {test_class_with_seed.new}

    it 'should have a name including the seed' do
      expect(subject.name).to start_with subject.class.name_seed
    end

    it 'should have a definitive name' do
      expect(subject.name).to eq subject.name
    end
  end

  context 'when changing the name' do

    context 'when no instance has the target name' do

      let(:new_name) { :a_non_existing_name }

      it 'should accept the change' do
        expect { subject.name = new_name }.not_to raise_error
        expect(test_class_with_no_seed.instances[new_name]).to eq subject
      end

    end

    context 'when an instance has already the target name' do

      let(:common_name) { :a_common_name }

      subject { test_class_with_no_seed.new }

      it 'should fail to rename it' do
        expect { subject.name = common_name }.not_to raise_error
        other = test_class_with_no_seed.new
        expect { other.name = common_name }.to raise_error UniverseCompiler::Error
      end

    end


  end
end