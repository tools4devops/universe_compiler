require 'spec_helper'

describe UniverseCompiler::Persistence::Management do

  let(:default_persistence_engine_name) { 'BasicYamlEngine' }

  let(:default_persistence_engine) { UniverseCompiler::Persistence::BasicYamlEngine }

  let(:test_class) do
    class ManagementTestClass
      include UniverseCompiler::Persistence::Management
    end
  end
  subject { test_class.new }

  it 'should return the list of persistence engines' do
    expect(subject.persistence_engines).to be_a Array
  end

  it 'should at least contain the default persistence engine' do
    expect(subject.persistence_engines).not_to be_empty
    expect(subject.persistence_engines).to include default_persistence_engine
  end

  it 'should have a default persistence engine' do
    expect(subject.persistence_engine).not_to be_nil
  end


end