require 'spec_helper'

describe 'Scenario 2 - Inheritance and override' do

  subject do
    entities = get_scenario2_valid_entities
    u = build(:universe)
    entities.each { |e| u.add e}
    u
  end

  before(:each) do
    UniverseCompiler::Universe::Base.universes.clear
  end

  context 'when inheritance is correctly set' do

    it 'should be valid' do
      expect(subject).to be_valid
      compiled = subject.compile
      e = compiled.get_entity :entity_override, 'Stupid Tagger'
      e.overrides.each do |override|
        expect(override).not_to be_nil
      end
      expect {compiled.valid? raise_error: true}.not_to raise_error
      x = compiled.get_entity :entity_override, 'Stupid Tagger'
      x.overrides.each do |override|
        expect(override).not_to be_nil
      end

    end


    it 'should not modify the original universe' do
      expect(subject).to be_valid
      subject.compile
      expect(subject).to be_valid
    end

    it 'should override correctly' do
      new_universe = nil
      expect {new_universe = subject.compile}.not_to raise_error
      org_release = subject.get_entity :images_release, 'Extra release'
      dest_release = new_universe.get_entity :images_release, 'Extra release'
      expect(dest_release.images_versions.keys.size).to be > org_release.images_versions.keys.size
      org_release.images_versions.each do |k, v|
        expect(dest_release.images_versions.keys).to include k
      end
      dest_release.images_versions.each do |k, v|
        unless org_release.images_versions.keys.include? k
          expect(org_release.extends.images_versions.keys).to include k
          org_release.extends.images_versions.keys.each do |key|
            expect(key).not_to be equal k
          end
        end
      end
    end



    context 'when a scenario is specified' do

      let(:scenario) { :tag_images_and_release }

      it 'should apply overrides' do
        new_universe = nil
        expect {new_universe = subject.compile scenario: scenario}.not_to raise_error
        images = new_universe.get_entities criterion: :by_type, value: :docker_image
        images.each do |image|
          expect(image[:tag]).to eq :yo
          expect(image.overridden_by).not_to be_empty
          expect(image.type).to be :docker_image
        end
        rel = new_universe.get_entity :images_release, 'Extra release'
        expect(rel[:tag]).to eq :yo
      end

    end

  end


end
