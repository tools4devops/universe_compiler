require 'spec_helper'

# class TestEntity < UniverseCompiler::Entity::Base
#
#   include UniverseCompiler::Entity::Overriding
#
#   entity_type :my_test_entity
#
#   has_one TestEntity
#   has_one :my_test_entity, name: :pal
#
#   not_null :pal
#   not_empty :siblings
#
#   has_many TestEntity
#   has_many TestEntity, name: :sibling
#
#   not_null :foo
#   should_match :foo, /^\d+$/
#
#   field :bar, :not_null, should_match: /.*/
#
# end

describe UniverseCompiler::Entity do

  subject {build :full_entity}

  it 'should have a type' do
    expect(subject).to respond_to :type
    expect(subject.type).to eq :my_test_entity
    expect(subject.class.entity_type).to eq :my_test_entity
  end

  it 'should not be possible to change the type' do
    expect {subject.class.entity_type = :another_entity_type}.to raise_error UniverseCompiler::Error
    expect(subject).not_to respond_to :'type='
  end

  it 'should have fields' do
    expect(subject).to respond_to :fields
  end

  context 'when an entity inherits from another one' do
    subject { InheritedTestEntity }
    let(:superclass_constraints) { %i(foo bar siblings pal) }
    let(:added_constraints) { %i(in_inherited) }

    it 'should inherit from superclass fields and constraints' do
      superclass_constraints.concat(added_constraints).each do |constraint|
        expect(subject.fields_constraints.keys).to include constraint
      end
    end
  end

  context 'when an entity type is auto-named' do

    subject { TestEntity2 }

    context 'when no universe is provided' do

      it 'should have a unique uuid automatically assigned to the name' do
        test_hash = {}
        (1..10).each do |_|
          s = subject.new
          test_hash[s.name] = true
        end
        expect(test_hash.size).to eq 10
      end

    end

    context 'when in  the context of a universe' do

      let(:universe) { UniverseCompiler::Universe::Base.new }

      it 'should have a unique sequenced name, build from the entity type' do
        test_hash = {}
        (1..10).each do |_|
          s = subject.new universe: universe
          test_hash[s.name] = true
        end
        expect(test_hash.size).to eq 10
      end

    end

  end


  context 'when defining constraints' do

    UniverseCompiler::Entity::FieldConstraintManagement::BOOLEAN_CONSTRAINTS.each do |constraint_name|
      it "should allow to add the constraint '#{constraint_name}'" do
        expect(subject.class).to respond_to constraint_name
      end
    end

    UniverseCompiler::Entity::FieldConstraintManagement::PARAMETRIZED_CONSTRAINTS.each do |constraint_name|
      it "should allow to define a value for the constraint '#{constraint_name}'" do
        expect(subject.class).to respond_to constraint_name
      end
    end

    it 'should have by default fields defined if there are constraints defined' do
      %i(foo bar).each do |field_name|
        expect(subject).to respond_to field_name
        expect(subject).to respond_to "#{field_name}="
      end
    end

  end

  context 'when there is an entity chain of inheritance' do

    context 'when links are permissive' do

      let(:level3) { Level3.new }
      let(:level2) { Level2.new }
      let(:root_level) { RootLevel.new }
      subject { RootLevel.new }

      it 'should accept inherited types' do
        expect(subject).to be_valid
        subject.permissive_link = root_level
        expect(subject).to be_valid
        subject.permissive_link = level2
        expect(subject).to be_valid
        subject.permissive_link = level3
        expect(subject).to be_valid
      end
    end

    context 'when links are strict' do

      let(:level3) { Level3.new }
      let(:level2) { Level2.new }
      let(:root_level) { RootLevel.new }
      subject { Level2.new }

      it 'should not accept inherited types' do
        expect(subject).to be_valid
        subject.strict_link = level2
        expect(subject).to be_valid
        subject.strict_link = level3
        expect(subject).not_to be_valid
        subject.strict_link = root_level
        expect(subject).not_to be_valid

        subject.strict_link = nil
        expect(subject).to be_valid

        subject.strict_links << level2
        expect(subject).to be_valid
        subject.strict_links << level3
        expect(subject).not_to be_valid
        subject.strict_links.pop
        expect(subject).to be_valid
        subject.strict_links << root_level
        expect(subject).not_to be_valid
      end

    end

  end


  context 'when accessing a field with accessor' do

    it 'should fail accessing a non existing field' do
      expect{subject.not_existing_field = :boo}.to raise_error NoMethodError
    end

    it 'should access an existing field' do
      expect{subject.foo = :boo}.not_to raise_error
      expect(subject.foo).to eq :boo
      expect{subject.bar = :boo}.not_to raise_error
      expect(subject.bar).to eq :boo
    end

  end


  context 'when defining relations' do

    it 'should translate a has_one relation into a singular field' do
      expect(subject).to respond_to :my_test_entity
      expect(subject).to respond_to :pal

    end

    it 'should translate a has_many relation into a plural field' do
      expect(subject).to respond_to :my_test_entities
      expect(subject).to respond_to :siblings
    end


  end

  context 'when validating an object' do

    it 'should be invalid by default (if some constraints do not match)' do
      expect(subject).not_to be_valid
    end

    it 'should raise an error if asked to' do
      expect { subject.valid? raise_error: true }.to raise_error UniverseCompiler::Error
    end

    context 'but when correctly filled' do

      it 'should be valid' do
        subject.foo = 1234
        expect(subject).not_to be_valid
        subject.bar = 'Yeah'
        expect(subject).not_to be_valid
        subject.pal = 'bla'
        expect(subject).not_to be_valid
        subject.pal = TestEntity.new
        expect(subject).not_to be_valid
        subject.siblings << TestEntity.new
        expect(subject).to be_valid
      end
    end


  end


end