require 'spec_helper'

describe UniverseCompiler::Package do

  let(:package_test_path) { nil }

  subject { described_class::Base.new package_test_path }

  it 'should be invalid by default' do
    expect(subject).not_to be_path_valid
  end

  context 'when specifying a correct path' do

    let(:package_test_path) { File.expand_path File.join('..', '..', 'test', 'test_package'), __FILE__}

    it 'should be valid' do
      expect(subject).to be_path_valid
    end

    it 'should be possible to load the package' do
      expect { subject.load }.not_to raise_error
      expect { EntityFoo.new }.not_to raise_error
    end

    it 'should be possible to load the package twice' do
      expect { subject.load }.not_to raise_error
    end

    context 'when specifying some invalid code' do

      let(:package_test_path) { File.expand_path File.join('..', '..', 'test', 'invalid_test_package'), __FILE__}

      it 'should be invalid' do
        expect { subject.load }.to raise_error UniverseCompiler::Error
      end

    end

  end




end