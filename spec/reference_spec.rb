require 'spec_helper'

describe UniverseCompiler::Entity::Reference do

  context 'when an entity is outside of the universe' do

    let (:release) {build(:release)}

    subject { release.to_reference }

    it 'should not be possible to resolve references ' do
      expect {subject.to_entity}.to raise_error UniverseCompiler::Error
    end
  end

  context 'when an entity is in the universe' do
    let (:universe) do
      u = build(:universe) << build(:docker_config)
      root = build(:base_image)
      u << root
      i = build(:inherited_from_base, from: root)
      u << i
      u << build(:release)
    end

    subject do
      universe.get_entity(:images_release, 'Stable release').to_reference
    end

    it 'should be possible to resolve references ' do
      expect {subject.to_entity}.not_to raise_error
    end

  end

  context 'when entities with has_one define reverse method' do
    let(:entity_a) { ReferencedTestEntity.new fields: {name: :a} }
    let(:entity_b) { OtherReferencedTestEntity.new fields: {name: :b} }

    let(:referencer) do
      r = TestEntityWithReference.new fields: {name: :referencer}
      r.ref_to_test_entity = entity_a
      r.another_ref_to_test_entity = entity_a
      r
    end

    let(:referencer2) do
      r = TestEntityWithReference.new fields: {name: :referencer2}
      r.another_ref_to_test_entity = entity_a
      r
    end

    subject do
      u = UniverseCompiler::Universe::Base.new
      u << entity_a << referencer << referencer2
    end

    it 'should have methods created on the referenced entity' do
      subject
      expect(entity_a).to respond_to :referenced_from_test_entity_with_reference_ref_to_test_entity
      expect(entity_a.referenced_from_test_entity_with_reference_ref_to_test_entity).to eq referencer
      expect(entity_a.referenced_from_test_entity_with_reference_ref_to_test_entity).not_to be_an Array
      expect(entity_a).to respond_to :ref_by
      expect(entity_a.ref_by).to be_an Array
      expect(entity_b).to respond_to :other_referenced_test_entity
      expect(entity_b).to respond_to :referenced_from_test_entity_with_reference_yop
      expect(entity_a.ref_by.size).to eq 2
      expect(entity_a.ref_by).to eq [referencer, referencer2]
    end


  end

  context 'when entities with has_many define reverse method' do
    let(:entity_a) { ReferencedTestEntity.new fields: {name: :a} }
    let(:entity_b) { ReferencedTestEntity.new fields: {name: :b} }
    let(:entity_c) { ReferencedTestEntity.new fields: {name: :c} }
    let(:entity_d) { ReferencedTestEntity.new fields: {name: :d} }

    let(:referencer) do
      r = TestEntityWithReference.new fields: {name: :referencer}
      r.ref_to_test_entity = entity_a
      r.foo << entity_a
      r.foo << entity_b
      r.foo << entity_c
      r
    end

    subject do
      u = UniverseCompiler::Universe::Base.new
      u << entity_a << entity_b << entity_c << referencer
    end

    it 'should have methods created on the referenced entity' do
      subject
      expect(entity_a).to respond_to :bar
      expect(entity_a.bar).to eq referencer
    end

    it 'should return nil if nothing is found through a unique reverse method' do
      subject << entity_d
      expect(entity_d.ref_by).to be_an Array
      expect(entity_d.ref_by).to be_empty
      expect(entity_d.bar).to be_nil

    end


  end



  context 'when manipulating universe content' do
    let(:entity_a) { ReferencedTestEntity.new fields: {name: :a} }
    let(:entity_b) { ReferencedTestEntity.new fields: {name: :b} }
    let(:entity_c) { ReferencedTestEntity.new fields: {name: :c} }
    let(:entity_d) { ReferencedTestEntity.new fields: {name: :d} }

    let(:referencer) do
      r = TestEntityWithReference.new fields: {name: :referencer}
      r.ref_to_test_entity = entity_a
      r.foo << entity_a
      r.foo << entity_b
      r.foo << entity_c
      r
    end

    subject do
      u = UniverseCompiler::Universe::Base.new
      u << entity_a << entity_b << entity_c << referencer
    end

    it 'should safely remove entities from a universe, keeping relations consistent and returning impact' do
      expect(referencer.ref_to_test_entity).to eq entity_a
      expect(referencer.foo.size).to be 3
      impacted_entities = []
      expect {impacted_entities = subject.delete entity_a }.not_to raise_error
      expect(impacted_entities.keys).to eq [referencer]
      expect(subject.get_entity :ref_to_test_entity, 'a').to be_nil
      expect(referencer.ref_to_test_entity).to be_nil
      expect(referencer.foo.size).to be 2
    end

    it 'should not allow to replace an entity with one already in the universe' do
      expect {subject.replace entity_b, entity_c }.to raise_error UniverseCompiler::Error
    end

    it 'should allow to replace an entity with one not existing in the universe' do
      impacted_entities = []
      expect {impacted_entities = subject.replace entity_a, entity_d }.not_to raise_error
      expect(impacted_entities.keys).to eq [referencer]
    end

  end



end
