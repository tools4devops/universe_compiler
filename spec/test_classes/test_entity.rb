class DockerConfig < UniverseCompiler::Entity::Base
  entity_type :docker_config

  field :registry, :not_empty
  field :image_namespace, :not_empty
end

class Image < UniverseCompiler::Entity::Base
  entity_type :docker_image

  has_one :docker_image, name: :from

  field :external
end

class Release < UniverseCompiler::Entity::Base
  entity_type :images_release

  has_one :docker_config
  not_null :docker_config

  is_hash :s2i_overrides
  is_hash :images_versions
end


class TestEntity < UniverseCompiler::Entity::Base

  entity_type :my_test_entity

  has_one TestEntity
  has_one :my_test_entity, name: :pal

  not_null :pal

  has_many TestEntity
  has_many TestEntity, name: :siblings
  not_empty :siblings

  not_null :foo
  should_match :foo, /^\d+$/

  field :bar, :not_null, should_match: /.*/
  field :stupid

end

class InheritedTestEntity < TestEntity

  entity_type :inherited_entity_type

  field :in_inherited

  has_one :second_level_inherited_entity_type, name: :strict_test, strict_type: true

end

class RootLevel < UniverseCompiler::Entity::Base
  entity_type :level1

  has_one :level1, name: :permissive_link

end

class Level2 < RootLevel
  entity_type :level2

  has_one :level2, name: :strict_link, strict_type: true
  has_many :level2, name: :strict_links, strict_type: true
end

class Level3 < Level2
  entity_type :level3
end



class TestEntity2 < UniverseCompiler::Entity::Base
  auto_named_entity_type
end


class ReferencedTestEntity < UniverseCompiler::Entity::Base
  entity_type :referenced_test_entity
end

class OtherReferencedTestEntity < UniverseCompiler::Entity::Base
  entity_type :other_referenced_test_entity
end


class TestEntityWithReference < UniverseCompiler::Entity::Base
  entity_type :test_entity_with_reference

  has_one :referenced_test_entity, name: :ref_to_test_entity, with_reverse_method: true, unique: true
  has_one ReferencedTestEntity, name: :another_ref_to_test_entity, with_reverse_method: :ref_by

  has_many :referenced_test_entity, name: :foo, with_reverse_method: :bar, unique: true

  has_one :other_referenced_test_entity, with_reverse_method: :other_referenced_test_entity, unique: true
  has_one :other_referenced_test_entity, name: :yop, with_reverse_method: true, unique: true

end
