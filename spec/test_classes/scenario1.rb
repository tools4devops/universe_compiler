
def get_scenario1_valid_entities
  dc = DockerConfig.new fields: {name: 'Your registry config',
                                 registry: 'your.registry:5000',
                                 image_namespace: 'ns_for_tests'
  }
  ri = Image.new fields: {name: 'root',
                          external: :busybox
  }
  ii = Image.new fields: {name: 'inherited',
                          from: ri
  }
  r = Release.new fields: {name: 'Stable release',
                           docker_config: dc,
                           images_versions: {
                               ri => '1.0.0',
                               ii => '0.2.1'
                           }
  }
  [dc, ri, ii, r]
end

def get_scenario1_invalid_entities
  get_scenario1_valid_entities.drop(1)
end



# OTHER_RELEASE = Release.new fields: {name: 'Extra release',
#                                      extends: RELEASE,
#                                      images_versions: {
#                                          ROOT_IMAGE => '1.0.1'
#                                      }
# }