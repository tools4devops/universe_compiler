
def get_scenario2_valid_entities
  dc = DockerConfig.new fields: {name: 'Your registry config',
                                 registry: 'your.registry:5000',
                                 image_namespace: 'ns_for_tests'
  }
  ri = Image.new fields: {name: 'root',
                          external: :busybox
  }
  ii = Image.new fields: {name: 'inherited',
                          from: ri
  }
  r = Release.new fields: {name: 'Stable release',
                           docker_config: dc,
                           images_versions: {
                               ri => '1.0.0',
                               ii => '0.2.1'
                           }
  }
  er = Release.new fields: {name: 'Extra release',
                            extends: r,
                            images_versions: {
                                ri => '1.0.1'
                            }
  }
  o = UniverseCompiler::Entity::Override.new fields:{name: 'Stupid Tagger',
                                                  scenario: :tag_images_and_release,
                                                  overrides: [ri, ii, er],
                                                  tag: :yo}


  [dc, ri, ii, r, er, o]
end

