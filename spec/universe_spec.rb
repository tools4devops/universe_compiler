require 'spec_helper'

class InvalidEntity ; end
class StillInvalidEntity
  attr_reader :name
end

# class ValidEntity < Struct.new(:name) ; end

class ValidEntity
  attr_accessor :name

  def initialize(name='default')
    self.name = name
  end
end

class AnotherValidEntity
  attr_accessor :name
  def self.entity_type
    :a_name
  end
end


describe UniverseCompiler::Universe::Base do
  let(:universe_name) {'Test Universe'}
  let (:invalid_entity) {InvalidEntity.new}
  let (:another_invalid_entity) {StillInvalidEntity.new}
  let (:valid_entity) {ValidEntity.new 'obj_name'}
  let (:valid_incomplete_entity) { AnotherValidEntity.new }
  let (:another_valid_entity) { e = AnotherValidEntity.new ; e.name = 'another_name' ; e }

  subject {described_class.new universe_name}

  before(:each) do
    described_class.universes.clear
  end

  it 'should should be empty by default' do
    expect(subject).to be_empty
  end

  it 'should not accept entities without name or type' do
    [invalid_entity, another_invalid_entity].each do |entity|
      expect {subject << entity}.to raise_error UniverseCompiler::Error
    end
  end

  it 'should accept valid entities' do
    expect {subject << valid_entity}.not_to raise_error
  end

  it 'should not accept the same valid object twice' do
    expect {subject << valid_entity}.not_to raise_error
    expect {subject << valid_entity}.to raise_error UniverseCompiler::Error
  end

  context 'when an entity is accepted' do

    it 'should have a type' do
      [valid_entity, another_valid_entity].each do |entity|
        expect { subject << entity }.not_to raise_error
        expect(entity).to respond_to :type
        expect(entity.class).to respond_to :entity_type
      end
    end

  end

  it 'should have basic criteria to find objects' do
    expect(subject.basic_criteria.size).to eq 3
  end


  context 'when using indices' do
    subject { described_class.new << valid_entity << another_valid_entity }

    it 'should return everything by default' do
      res = nil
      expect {res = subject.get_entities}.not_to raise_error
      expect(res).to be_a Array
      expect(res).not_to be_empty
      expect(res.size).to be 2
    end

    it 'should allow searching by name' do
      res = nil
      expect {res = subject.get_entities criterion: :by_name, value: 'obj_name'}.not_to raise_error
      expect(res).to be_a Array
      expect(res).not_to be_empty
      expect {res = subject.get_entities criterion: :by_name, value: 'not_existing'}.not_to raise_error
      expect(res).to be_a Array
      expect(res).to be_empty
    end

    it 'should allow searching by type' do
      res = nil
      expect { res = subject.get_entities criterion: :by_type, value: 'valid_entity' }.not_to raise_error
      expect(res).to be_a Array
      expect(res).not_to be_empty
      expect { res = subject.get_entities criterion: :by_type, value: 'not_existing' }.not_to raise_error
      expect(res).to be_a Array
      expect(res).to be_empty
    end

    it 'should allow searching by uniq key' do
      res = nil
      expect { res = subject.get_entities criterion: :by_uniq_key, value: %W(valid_entity obj_name) }.not_to raise_error
      expect(res).to be_a Array
      expect(res).not_to be_empty
      expect { res = subject.get_entities criterion: :by_uniq_key, value: :not_an_id }.not_to raise_error
      expect(res).to be_a Array
      expect(res).to be_empty
    end

    it 'should support filtering' do
      res, count = nil, 0
      expect do
        res = subject.get_entities do |entity|
          count += 1
          true
        end
        expect(res).to be_a Array
        expect(res.size).to eq count
      end.not_to raise_error
      expect do
        res = subject.get_entities do |entity|
          count -= 1
          false
        end
        expect(res).to be_a Array
        expect(res).to be_empty
        expect(res.size).to eq count
      end.not_to raise_error

    end

  end

  context 'when dealing with persistence' do

    let(:yaml_storage_path) { File.expand_path File.join('..', '..', 'test', 'test_yaml_storage'), __FILE__ }

    it 'should be possible to import a universe' do
      expect { subject.import yaml_storage_path, force: true }.not_to raise_error
    end

    it 'should be possible to export a universe' do
      subject.import yaml_storage_path, force: true
      Dir.mktmpdir do |dir|
        expect { subject.export dir }.not_to raise_error
        expect { subject.import dir, force: true }.not_to raise_error
      end
    end

    it 'should fail when importing entities already existing in the universe' do
      expect { subject.import yaml_storage_path, force: true }.not_to raise_error
      expect { subject.import yaml_storage_path, force: true }.not_to raise_error
      expect { subject.import yaml_storage_path }.to raise_error UniverseCompiler::Error
    end

  end

end