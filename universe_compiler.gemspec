# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'universe_compiler/version'

Gem::Specification.new do |spec|
  spec.name          = 'universe_compiler'
  spec.version       = UniverseCompiler::VERSION
  spec.authors       = ['Laurent B.']
  spec.email         = ['lbnetid+rb@gmail.com']

  spec.summary       = 'Manage a set of entities with complex interactions.'
  spec.description   = 'Provide a way to create objects with complex relationships, inheritance and override mechanisms.'
  spec.homepage      = 'https://gitlab.com/tools4devops/universe_compiler'
  spec.license       = 'MIT'

  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_development_dependency 'bundler', '~> 2.0'
  spec.add_development_dependency 'factory_girl', '~> 4.0', '>= 4.8'
  spec.add_development_dependency 'pry'
  spec.add_development_dependency 'rake', '~> 10.0'
  spec.add_development_dependency 'rspec', '~> 3.0'
  spec.add_development_dependency 'rubocop'

  spec.add_runtime_dependency 'activesupport', '~> 5.0'
  spec.add_runtime_dependency 'ruby-graphviz'
  spec.add_runtime_dependency 'super_stack', '~> 1.0', '>= 1.0.6'

end
