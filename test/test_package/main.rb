class EntityFoo < UniverseCompiler::Entity::Base
  entity_type :foo

  field :registry, :not_empty
  field :image_namespace, :not_empty
end

class EntityBar < UniverseCompiler::Entity::Base
  entity_type :bar

  has_one :foo, name: :daddy
  has_many :foo, name: :kids
end
