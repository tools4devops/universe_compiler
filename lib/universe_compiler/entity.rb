module UniverseCompiler
  module Entity

    class Base

      include UniverseCompiler::Utils::ErrorPropagation
      include UniverseCompiler::Utils::DeepTraverse
      include UniverseCompiler::Entity::TypeManagement
      include UniverseCompiler::Entity::FieldManagement
      extend UniverseCompiler::Entity::AutoNamed
      extend UniverseCompiler::Entity::FieldConstraintManagement
      extend UniverseCompiler::Entity::RelationsManagement
      extend UniverseCompiler::Entity::FieldBinder
      include UniverseCompiler::Entity::Validation
      include UniverseCompiler::Entity::Marshalling
      include UniverseCompiler::Entity::Inheritance
      include UniverseCompiler::Entity::Conversion
      include UniverseCompiler::Entity::Overridden
      include UniverseCompiler::Entity::Persistence

      attr_reader :fields, :universe

      field_accessor :name

      def initialize(fields: {}, universe: nil)
        @fields = fields
        define_known_fields_accessors
        define_reverse_methods
        self.universe = universe
        self.fully_resolved = true
        if universe.nil?
          self.name = self.class.get_unique_name(nil) if self.class.auto_named_entity_type?
        else
          unless universe.compiled?
            self.name = self.class.get_unique_name(universe) if self.class.auto_named_entity_type?
          end
        end
      end

      def universe=(a_universe)
        unless fully_resolved?
          deep_traverse fields do |leaf|
            leaf.universe = a_universe if leaf.is_a? UniverseCompiler::Entity::Reference
          end
        end
        @universe = a_universe
      end

      def [](key)
        fields[key]
      end

      def []=(key, value)
        fields[key] = value
      end

    end

  end
end



















