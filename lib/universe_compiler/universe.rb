require 'active_support/core_ext/string/inflections'

require 'universe_compiler/universe/query'

module UniverseCompiler
  module Universe

    class Base

      include UniverseCompiler::Utils::ErrorPropagation
      extend UniverseCompiler::Universe::Multiverse
      include UniverseCompiler::Universe::Entities
      include UniverseCompiler::Universe::Index
      include UniverseCompiler::Universe::Query
      include UniverseCompiler::Universe::Validation
      include UniverseCompiler::Universe::Duplication
      include UniverseCompiler::Universe::Compile
      include UniverseCompiler::Universe::Persistence

      attr_reader :name

      def initialize(name = self.class.get_unique_name)
        @name = self.class.get_unique_name name
        @entities = []
        self.class.register self
        setup_indices
      end

      def name=(value)
        self.class.universes.delete name
        @name = self.class.get_unique_name value
        self.class.register self
      end

      def compiled?
        @compiled
      end

      protected

      attr_writer :compiled

    end

  end
end