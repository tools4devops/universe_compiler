module UniverseCompiler
  module Entity

    class Override < UniverseCompiler::Entity::Base

      UNMERGEABLE_FIELDS = %i(name type overrides scenario extends)

      entity_type :entity_override

      field :overrides, :is_array
      field_accessor :scenario

      def apply_overrides
        overrides.each do |override|
          fields_to_be_merged = fields.reject { |key, _| UNMERGEABLE_FIELDS.include? key }
          UniverseCompiler.logger.debug "Overriding '#{override.to_composite_key}' from overrides defined in '#{to_composite_key}'."
          override.apply_override fields_to_be_merged, self
        end
      end

    end

  end
end
