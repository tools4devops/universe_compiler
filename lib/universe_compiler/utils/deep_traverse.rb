module UniverseCompiler
  module Utils

    module DeepTraverse

      def deep_traverse(structure, &block)
        case structure
        when Hash
          deep_traverse_hash structure, &block
        when Array
          deep_traverse_array structure, &block
        else
          yield structure
        end
      end

      def deep_map(structure, &block)
        case structure
        when Hash
          deep_map_hash structure, &block
        when Array
          deep_map_array structure, &block
        else
          yield structure
        end
      end

      private

      def deep_traverse_hash(hash, &block)
        hash.each do |k, v|
          deep_traverse k, &block
          deep_traverse v, &block
        end
      end

      def deep_traverse_array(array, &block)
        array.each.with_index do |v, idx|
          deep_traverse v, &block
        end
      end

      def deep_map_hash(hash, res = {}, &block)
        hash.each do |k, v|
          k = deep_map k, &block
          res[k] = deep_map v, &block
        end
        res
      end

      def deep_map_array(array, res = [], &block)
        array.each.with_index do |v, idx|
          res[idx] = deep_map v, &block
        end
        res
      end

    end

  end
end