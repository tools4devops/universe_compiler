module UniverseCompiler
  module Utils

    module ErrorPropagation

      private

      def false_or_raise(message = 'Error message not provided!', raise_error: false)
        if raise_error
          raise UniverseCompiler::Error, message
        else
          UniverseCompiler.logger.warn message
        end
        false
      end

    end

  end
end
