module UniverseCompiler
  module Utils

    module ArrayUtils


      def contains_only_one_of?(*choices)
        used = used_choices *choices
        return true, used if used.size == 1
        return false, used
      end

      def contains_at_least_one_of?(*choices)
        used = used_choices *choices
        return true, used unless used.empty?
        return false, used
      end

      def contains_all_of?(*choices)
        used = used_choices *choices
        return true, used if used.size == choices.size
        return false, used
      end

      def raises_or_logs(msg, raise_exception=false, logger_level=:error)
        if raise_exception then
          raise UniverseCompiler::Error, msg
        else
          UniverseCompiler.logger.send logger_level, msg
          return false
        end
      end

      private

      def used_choices(*choices)
        choices.map do |choice|
          choice_content = self.send choice
          case choice_content
            when NilClass
              nil
            when Fixnum
              choice
            when String
              choice_content.empty? ? nil : choice
            when Array
              choice_content.empty? ? nil : choice
            when Hash
              choice_content.empty? ? nil : choice
          end
        end .compact
      end



    end

  end
end
