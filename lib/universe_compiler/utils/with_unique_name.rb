module UniverseCompiler
  module Utils

    module WithUniqueName

      module ClassMethods

        DEFAULT_SEED = 'Unknown'.freeze

        def set_name_seed(name_seed)
          @name_seed = name_seed.to_s
        end

        def name_seed
          @name_seed ||DEFAULT_SEED
        end

        def instances
          @instances ||= {}
          @instances.dup
        end

        def new_instance(name, *args)
          check_name name
          new_instance = new *args
          @instance[name] = new_instance
          new_instance
        end

        def set_name(name, object)
          check_name name
          old_name = instances.key object
          @instances[old_name] = nil unless old_name.nil?
          @instances[name] = object
        end

        def get_unique_name
          '%s_%d' % [name_seed, instance_counter]
        end

        private

        def instance_counter
          @instance_counter ||= 0
          @instance_counter += 1
          @instance_counter
        end

        def check_name(name)
          raise UniverseCompiler::Error, 'There is already an instance of %s named %s' % [self, name] unless instances[name].nil?
        end

      end

      def name
        defined_name = self.class.instances.key self
        if defined_name.nil?
          defined_name = self.class.get_unique_name
          self.name = defined_name
        end
        defined_name
      end

      def name=(name)
        self.class.set_name name, self
      end

      def self.included(base)
        base.extend UniverseCompiler::Utils::WithUniqueName::ClassMethods
      end

    end

  end
end