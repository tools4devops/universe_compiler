module UniverseCompiler

  class Error < StandardError

    attr_accessor :original

    def self.from(exception, message = exception.message)
      wrapper = new message
      wrapper.original = exception
      wrapper
    end

  end

end
