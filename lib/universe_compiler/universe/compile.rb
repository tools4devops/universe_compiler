module UniverseCompiler
  module Universe

    module Compile

      include UniverseCompiler::Utils::DeepTraverse

      def compile(scenario: nil)
        valid? raise_error: true
        UniverseCompiler.logger.info "Starting compilation of universe '#{name}'"
        # Pass 1 - Universe duplication
        target_universe = dup

        # Pass 2 - Applying entities inheritance
        target_universe.apply_entities_inheritance

        # Apply scenario
        target_universe.apply_entities_overrides scenario unless scenario.nil?

        UniverseCompiler.logger.info "Completed compilation of universe '#{name}' into '#{target_universe.name}'"
        target_universe
      end

      protected

      def apply_entities_overrides(scenario)
        UniverseCompiler.logger.debug "Starting override process for universe '#{name}'."
        candidates = by_type.fetch(UniverseCompiler::Entity::Override.entity_type, []).select { |overrider| overrider.scenario == scenario }
        UniverseCompiler.logger.warn "No override found for scenario '#{scenario}' in universe '#{name}'." if candidates.empty?
        candidates.each(&:apply_overrides)
        UniverseCompiler.logger.debug "Completed override process for universe '#{name}'."
        self
      end

      def apply_entities_inheritance
        UniverseCompiler.logger.debug "Starting inheritance process for universe '#{name}'."
        entities.each(&:apply_inheritance)
        UniverseCompiler.logger.debug "Completed inheritance process for universe '#{name}'."
        self
      end

    end

  end
end