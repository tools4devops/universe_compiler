require 'securerandom'

module UniverseCompiler
  module Universe

    module Multiverse

      DEFAULT_UNIVERSE_NAME = 'Unnamed Universe'.freeze

      def universes
        @universes ||= {}
      end

      def register(universe)
        raise UniverseCompiler::Error, "Universe '#{universe.name}' already exists in this continuum !" if universes.keys.include? universe.name
        universes[universe.name] = universe
      end

      def get_unique_name(seed = DEFAULT_UNIVERSE_NAME)
        if universes[seed]
          format_name seed, SecureRandom.uuid
        else
          seed
        end
      end

      private

      def format_name(name, uuid)
        '%s - #%s' % [name, uuid]
      end

    end

  end
end

