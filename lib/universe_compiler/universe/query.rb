module UniverseCompiler
  module Universe

    module Query

      def basic_criteria
        self.private_methods.grep /^by_/
      end

      def get_entity(type, name)
        res = get_entities criterion: :by_uniq_key, value: [type, name]
        res.empty? ? nil : res.first
      end

      def get_entities(criterion: nil, value: nil, &filter_block)
        res = if criterion.nil? then
                entities.clone
              else
                raise "Invalid criterion '#{criterion}' !" unless basic_criteria.include? criterion
                if value.nil?
                  self.send(criterion).clone
                else
                  self.send(criterion).clone[value]
                end
              end
        res = case res
                when NilClass
                  []
                when Array
                  res
                else
                  [res]
              end
        if block_given?
          res.select! do |entity|
            filter_block.call entity
          end
        end
        res
      end

    end

  end
end