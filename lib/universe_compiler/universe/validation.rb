module UniverseCompiler
  module Universe

    module Validation

      def valid?(raise_error: false)
        entities.each do |entity|
          UniverseCompiler.logger.debug "Checking '#{entity.to_composite_key}'"
          deep_map entity.fields do |leaf|
            case leaf
            when UniverseCompiler::Entity::Reference
              return false_or_raise "Entity '#{entity.to_composite_key}' contains an invalid reference to '#{leaf.to_composite_key}' !", raise_error: raise_error
            when UniverseCompiler::Entity::Base
              unless leaf.universe == self
                return false_or_raise "Entity '#{leaf.to_composite_key}' is not in the correct universe !", raise_error: raise_error
              end
              unless entities.include? leaf
                return false_or_raise "Entity '#{leaf.to_composite_key}' is not present in unniverse !", raise_error: raise_error
              end
            end
          end
          entity.valid?
        end
        true
      end

    end

  end
end