module UniverseCompiler
  module Universe

    module Persistence

      include UniverseCompiler::Persistence::Management

      def export(uri)
        engine = persistence_engine.new self, uri
        engine.export_universe
      end

      def import(uri, force: false, stop_on_error: true, &block)
        engine = persistence_engine.new self, uri
        clear if force
        engine.import_universe recursive: true, stop_on_error: stop_on_error, &block
        resolve_entities_reference
      end

    end

  end
end
