module UniverseCompiler
  module Universe

    module Index

      private

      attr_reader :by_name, :by_type, :by_uniq_key

      def setup_indices
        @by_name = {}
        @by_type = {}
        @by_uniq_key = {}
      end
      alias_method :clear_indices, :setup_indices


      def index(entity)
        # Non unique indices
        by_name[entity.name] ||= []
        by_type[entity.type] ||= []
        by_name[entity.name] << entity
        by_type[entity.type] << entity
        # Unique index
        ref = entity.respond_to?(:to_composite_key) ? entity.to_composite_key : [entity.type, entity.name]
        by_uniq_key[ref] = entity
      end

      def reindex_all(entities)
        clear_indices
        entities.each do |entity|
          index entity
        end
      end

    end

  end
end

