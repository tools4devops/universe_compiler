module UniverseCompiler
  module Persistence

    module Management

      DEFAULT_ENGINE_NAME = 'BasicYamlEngine'.freeze

      def persistence_engine
        if @persistence_engine.nil?
          self.persistence_engine_name = DEFAULT_ENGINE_NAME
        end
        @persistence_engine
      end

      def persistence_engines
        UniverseCompiler::Persistence.constants.map(&:to_s).grep(/Engine$/).map do |engine_name|
          self.persistence_engine_name = engine_name
          persistence_engine
        end
      end

      def persistence_engine_name=(engine_name)
        @persistence_engine = UniverseCompiler::Persistence.const_get engine_name
        @persistence_engine_name = engine_name
      end

    end

  end
end
