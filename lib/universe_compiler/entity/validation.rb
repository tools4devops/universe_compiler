module UniverseCompiler
  module Entity

    module Validation

      def invalid_fields
        invalid = {}
        self.class.fields_constraints.each do |field_name, constraints|
          constraints.each do |constraint_name, value|
            case constraint_name
            when :not_null
              invalid_for_constraint invalid, field_name, constraint_name, value if fields[field_name].nil?
            when :not_empty
              invalid_for_constraint invalid, field_name, constraint_name, value if fields[field_name].nil? or fields[field_name].empty?
            when :should_match
              unless fields[field_name].nil?
                invalid_for_constraint invalid, field_name, constraint_name, value unless fields[field_name].to_s.match value
              end
            when :class_name
              unless fields[field_name].nil?
                case value
                when String
                  invalid_for_constraint invalid, field_name, constraint_name, value unless fields[field_name].class.name == value
                else
                  invalid_for_constraint invalid, field_name, constraint_name, value unless fields[field_name].class == value
                end
              end
            when :is_array
              if fields[field_name].nil? or not fields[field_name].is_a? Array
                invalid_for_constraint invalid, field_name, constraint_name, value
              end
            when :is_hash
              if fields[field_name].nil? or not fields[field_name].is_a? Hash
                invalid_for_constraint invalid, field_name, constraint_name, value
              end
            when :has_one
              unless fields[field_name].nil?
                unless provided_entity_compatible_with_type? fields[field_name], value, constraints[:strict_type]
                  invalid_for_constraint invalid, field_name, constraint_name, value
                end
              end
            when :has_many
              if fields[field_name].nil? or not fields[field_name].is_a? Array
                invalid_for_constraint invalid, field_name, constraint_name, value
              else
                fields[field_name].each do |related_object|
                  unless provided_entity_compatible_with_type? related_object, value, constraints[:strict_type]
                    invalid_for_constraint invalid, field_name, constraint_name, value
                  end
                end
              end
            when :reverse_method
              begin
                send value[:actual_method] if value[:unique_result]
              rescue UniverseCompiler::Error => uce
                invalid_for_constraint invalid, value[:actual_method], constraint_name, value
              end
            else
              UniverseCompiler.logger.warn "Cannot handle unknown constraint '#{constraint_name}'! Skipping..."
            end
          end
        end
        invalid
      end

      def valid_for_fields_constraints?(raise_error: false)
        return true if invalid_fields.empty?
        invalid_fields.each do |name,value|
          UniverseCompiler.logger.error "Invalid field '#{name}' not compliant with '#{value}'"
        end
        fields_labels = invalid_fields.keys.join ', '
        false_or_raise "Invalid entity '#{to_composite_key}' for fields #{fields_labels} !", raise_error: raise_error
      end

      def valid?(raise_error: false)
        return false unless valid_for_fields_constraints? raise_error: raise_error
        return false unless valid_for_inheritance? raise_error: raise_error
        if respond_to? :specifically_valid?
          return false unless specifically_valid? raise_error: raise_error
        end
        true
      end

      private

      def provided_entity_compatible_with_type?(linked_object, declared_type, strict_type)
        declared_class = UniverseCompiler::Entity::TypeManagement.type_class_mapping(declared_type)
        if strict_type
          linked_object.class == declared_class
        else
          linked_object.is_a? declared_class
        end
      end

      def invalid_for_constraint(invalid_fields_definition, field_name, constraint_name, value)
        invalid_fields_definition[field_name] ||= []
        invalid_fields_definition[field_name] << { constraint_name => value}
      end

    end

  end
end
