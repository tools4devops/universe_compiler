require 'fileutils'

module UniverseCompiler
  module Entity

    module Persistence

      attr_accessor :source_uri

      def self.load(uri)
        entity = YAML.load_file uri
        entity.source_uri = uri
        entity
      end

      def save(uri = source_uri, raise_error: true, force_save: false)
        if force_save
          if valid? raise_error: false
            UniverseCompiler.logger.debug "Forcing '#{self.as_path}' save."
          else
            UniverseCompiler.logger.warn "Forcing '#{self.as_path}' save despite it is invalid !"
          end
        else
          valid? raise_error: raise_error
        end
        FileUtils.mkpath File.dirname(uri)
        File.write uri, to_yaml, mode: 'w'
        self.source_uri = uri
        UniverseCompiler.logger.debug "Saved '#{self.as_path}' to '#{uri}'"
        self
      end

      def delete
        universe.delete self
        unless self.source_uri.nil?
          FileUtils.rm self.source_uri
          UniverseCompiler.logger.debug "Deleted '#{self.as_path}' (removed file '#{self.source_uri}'"
        end
      end

    end

  end
end