module UniverseCompiler
  module Entity

    module Overridden

      def overridden_by
        @overridden_by ||= []
      end

      def apply_override(override_fields, overrider)
        merge_engine = SuperStack::Manager.new
        merge_engine.merge_policy = SuperStack::MergePolicies::InheritanceMergePolicy
        merge_engine << fields.to_hash
        merge_engine << override_fields
        add_overrider overrider
        @fields = merge_engine[]
        merge_engine.clear_layers
        @fields
      end

      private

      def add_overrider(overrider)
        raise "This object #{to_composite_key} is already overridden by #{overrider.to_composite_key}" if overridden_by.include? overrider
        overridden_by << overrider
      end

    end

  end
end
