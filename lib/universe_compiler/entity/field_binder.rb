module UniverseCompiler
  module Entity

    module FieldBinder

      def field_accessor(*field_names)
        field_names.each do |field_name|
          field_reader field_name
          field_writer field_name
        end
      end

      def field_reader(*field_names)
        field_names.each do |field_name|
          self.class_eval do
            define_method field_name do
              self.fields[field_name]
            end
          end
        end
      end

      def field_writer(*field_names)
        field_names.each do |field_name|
          self.class_eval do
            define_method "#{field_name}=" do |val|
              self.fields[field_name] = val
            end
          end
        end
      end

    end

  end
end