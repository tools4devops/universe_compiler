
module UniverseCompiler
  module Entity

    module AutoNamed

      attr_reader :auto_named_entity_type_seed

      def auto_named_entity_type(seed = nil)
        @auto_named_entity_type = true
        @auto_named_entity_type_seed = if seed.nil?
                                         entity_type.to_s
                                       else
                                         seed
                                       end
        @entity_type_counter = 0
      end

      def auto_named_entity_type?
        @auto_named_entity_type
      end

      def get_unique_name(universe)
        return SecureRandom.uuid if universe.nil?

        uniq_name = nil
        loop do
          @entity_type_counter += 1
          raise UniverseCompiler::Error, "Too many '#{entity_type}' (> 999999) in universe '#{universe.name}'" if @entity_type_counter >= 1000000
          uniq_name = '%s_%06u' % [auto_named_entity_type_seed, @entity_type_counter]
          break if universe.get_entity(entity_type, uniq_name).nil?
        end
        uniq_name
      end

    end

  end
end
