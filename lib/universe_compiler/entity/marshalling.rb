module UniverseCompiler
  module Entity

    module Marshalling


      def fully_resolved?
        fully_resolved || false
      end

      def resolve_fields_references!(from_fields = fields)
        UniverseCompiler.logger.debug "Starting resolution for '#{to_composite_key}'."
        @fields = resolve_fields_references from_fields
        UniverseCompiler.logger.debug "Completed resolution for '#{to_composite_key}'."
        self
      end

      def resolve_fields_references(from_fields = fields)
        self.fully_resolved = true
        deep_map from_fields do |leaf|
          case leaf
          when UniverseCompiler::Entity::Reference
            res = leaf.to_entity raise_error: false
            if res
              res
            else
              self.fully_resolved = false
              leaf
            end
          else
            leaf
          end
        end
      end

      def traverse_fields(fields_to_process = fields)
        deep_traverse(fields_to_process) do |leaf|
          yield leaf
        end
      end

      def dereferenced_fields(fields_to_dereference = fields)
        deep_map(fields_to_dereference) do |leaf|
          case leaf
          when UniverseCompiler::Entity::Base
            leaf.to_reference
          when Symbol, Numeric, NilClass, TrueClass, FalseClass
            leaf
          else
            leaf.clone
          end
        end
      end

      private

      attr_accessor :fully_resolved

      def marshal_dump
        { fields: dereferenced_fields, universe: universe.name }
      end

      def marshal_load(data)
        stored_universe = UniverseCompiler::Universe::Base.universes[data[:universe]]
        initialize fields: data[:fields], universe: stored_universe
      end
    end

  end
end

