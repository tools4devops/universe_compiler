module SuperStack
  module MergePolicies

    module InheritanceMergePolicy

      extend UniverseCompiler::Utils::DeepTraverse
      extend UniverseCompiler::Entity::Marshalling

      def self.merge(h1, h2)
        h1_dereferenced = dereferenced_fields h1
        h2_dereferenced = dereferenced_fields h2
        merged = h1_dereferenced.deep_merge! h2_dereferenced
        resolve_fields_references merged
      end

    end

  end
end