require 'digest'

module UniverseCompiler
  module Entity

    module Conversion

      def as_path
        to_composite_key.map(&:to_s).join '/'
      end

      def to_composite_key
        [type, name]
      end

      def to_uniq_id
        Digest::SHA256.hexdigest to_composite_key.join ':'
      end

      def inspect
        msg = "#<#{self.class.name}:#{object_id} composite_key=#{to_composite_key.inspect}"
        msg << ", @universe='#{universe.name}'" unless universe.nil?
        msg << '>'
        msg
      end

      def ==(another_reference)
        return false unless another_reference.respond_to? :to_composite_key
        to_composite_key == another_reference.to_composite_key
      end

      def eql?(another_reference)
        self == another_reference and universe == another_reference.universe
      end

      def to_hash
        {
            self.class.name => {
                type: type,
                fields: dereferenced_fields
            }
        }
      end

      def to_reference
        UniverseCompiler::Entity::Reference.new_instance self
      end

      def encode_with(coder)
        dereferenced_fields.each do |key, value|
          coder[key] = value
        end
      end

      def init_with(coder)
        initialize
        self.fully_resolved = false
        coder.map.each do |key, value|
          self[key] = value
        end
      end

    end

  end
end
