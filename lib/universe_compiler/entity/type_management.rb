module UniverseCompiler
  module Entity

    module TypeManagement

      # **
      # Methods to be added into the class the UniverseCompiler::Entity::TypeManagement is included in.
      module ClassMethods

        def entity_type(value = nil)
          if value.nil?
            @entity_type || name.underscore
          else
            self.entity_type = value
          end
        end

        def entity_type=(value)
          return if value == @entity_type
          raise UniverseCompiler::Error, "You cannot change an entity type for class '#{self.name}'" unless @entity_type.nil?
          raise UniverseCompiler::Error, 'Only Symbol is supported for entity_type !' unless value.is_a? Symbol
          mapping = UniverseCompiler::Entity::TypeManagement.types_classes_mapping
          if mapping.keys.include? value
            raise UniverseCompiler::Error, "Type '#{value}' already registered for another class (#{mapping[value]})" unless self == mapping[value]
          end
          mapping[value] = self
          @entity_type = value
        end

        private

        def normalize_entity_type(entity_type_or_class)
          case entity_type_or_class
          when Class
            entity_type_or_class.entity_type
          when Symbol, String
            entity_type_or_class.to_sym
          end
        end

      end

      # **
      # Module methods to be directly used

      def self.type_class_mapping(type_or_class)
        case type_or_class
        when Symbol, String
          types_classes_mapping[type_or_class.to_sym]
        when Class
          type_or_class
        end
      end

      def self.types_classes_mapping
        @types_classes_mapping ||= {}
      end

      def self.valid_for_type?(entity)
        entity.respond_to? :type and entity.class.respond_to? :entity_type
      end

      def self.included(base)
        base.extend(ClassMethods)
      end

      def self.extended(base)
        included base.class
      end

      # **
      # The only instance method

      def type
        self.class.entity_type
      end

    end

  end
end