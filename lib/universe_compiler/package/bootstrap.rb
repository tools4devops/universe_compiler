module UniverseCompiler
  module Package

    module Bootstrap

      DEFAULT_BOOTSTRAP_FILE = 'main.rb'.freeze

      attr_reader :path

      def path=(path)
        @path = path if path_valid? path
      end

      def path_valid?(path = self.path)
        return false unless path.is_a?(String) && File.readable?(path)
        return true if File.file? path
        File.exist? default_bootstrap_file(path)
      end

      def bootstrap_file
        return nil unless path_valid?
        if File.file? path
          path
        else
          default_bootstrap_file
        end
      end

      def load
        require bootstrap_file
      rescue ScriptError => e
        msg = "Invalid package: '#{bootstrap_file}': #{e.message}"
        UniverseCompiler.logger.error msg
        raise UniverseCompiler::Error.from(e, msg)
      end

      private

      def default_bootstrap_file(path = self.path)
        File.join(path, DEFAULT_BOOTSTRAP_FILE)
      end

    end

  end
end