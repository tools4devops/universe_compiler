module UniverseCompiler

  module Package

    class Base

      include UniverseCompiler::Utils::WithUniqueName
      include UniverseCompiler::Package::Bootstrap

      def initialize(path = nil)
        self.path = path
      end

    end

  end
end