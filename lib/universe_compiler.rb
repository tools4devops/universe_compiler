require 'super_stack'
require 'graphviz'

require 'universe_compiler/version'
require 'universe_compiler/error'
require 'universe_compiler/utils/basic_logger'
require 'universe_compiler/utils/error_propagation'
require 'universe_compiler/utils/array_utils'
require 'universe_compiler/utils/deep_traverse'
require 'universe_compiler/utils/with_unique_name'
require 'universe_compiler/utils/graphviz'

module UniverseCompiler
  extend UniverseCompiler::Utils::BasicLogger
end

# Entity
require 'universe_compiler/entity/type_management'
require 'universe_compiler/entity/auto_named'
require 'universe_compiler/entity/conversion'
require 'universe_compiler/entity/reference'
require 'universe_compiler/entity/marshalling'
require 'universe_compiler/entity/field_binder'
require 'universe_compiler/entity/field_management'
require 'universe_compiler/entity/field_constraint_management'
require 'universe_compiler/entity/relations_management'
require 'universe_compiler/entity/validation'
require 'universe_compiler/entity/inheritance_merge_policy'
require 'universe_compiler/entity/inheritance'
require 'universe_compiler/entity/overridden'
require 'universe_compiler/entity/persistence'
require 'universe_compiler/entity'
require 'universe_compiler/override'
# Persistence
require 'universe_compiler/persistence/basic_yaml_engine'
require 'universe_compiler/persistence/management'
# Universe
require 'universe_compiler/universe/multiverse'
require 'universe_compiler/universe/entities'
require 'universe_compiler/universe/index'
require 'universe_compiler/universe/query'
require 'universe_compiler/universe/validation'
require 'universe_compiler/universe/duplication'
require 'universe_compiler/universe/compile'
require 'universe_compiler/universe/persistence'
require 'universe_compiler/universe'
# Packages
require 'universe_compiler/package/bootstrap'
require 'universe_compiler/package'


module UniverseCompiler

  def self.new_universe(*args)
    UniverseCompiler::Universe::Base.new *args
  end

  def self.new_override(*args)
    UniverseCompiler::Entity::Override.new *args
  end

end